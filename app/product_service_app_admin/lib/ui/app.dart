import 'package:flutter/material.dart';
import 'package:product_service_app_admin/di/di.dart';
import 'package:product_service_app_admin/res/str/s.dart';
import 'package:product_service_app_admin/ui/navigator/app_navigator.dart';
import 'package:provider/provider.dart';

Widget app() {
  return FutureBuilder(
    future: Di.init(),
    builder: (context, snap) {
      if (snap.connectionState == ConnectionState.done) {
        return App();
      } else {
        return Container();
      }
    },
  );
}

class App extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => AppState();
}

class AppState extends State<App> {
  AppNavigator appNavigator;
  final key = GlobalKey<NavigatorState>();

  @override
  void initState() {
    appNavigator = Di.instance.appNavigator();
    appNavigator.key = key;
    super.initState();
  }

  @override
  void dispose() {
    appNavigator.key = null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Provider(
      create: (BuildContext context) => S(),
      child: MaterialApp(
        navigatorKey: key,
        onGenerateRoute: appNavigator.loginRoute,
      ),
    );
  }
}
