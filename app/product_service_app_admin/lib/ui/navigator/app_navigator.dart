import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:product_service_app_admin/ui/scene/auth/auth_scene.dart';

class AppNavigator {
  GlobalKey<NavigatorState> key;

  NavigatorState get _state => key.currentState;

  Route loginRoute(RouteSettings settings) {
    return AuthScene().route();
  }
}

extension _Route on Widget {
  Route route() {
    return MaterialPageRoute(builder: (_) => this);
  }
}
