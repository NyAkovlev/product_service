import 'package:flutter/material.dart';
import 'package:product_service_app_admin/di/di.dart';
import 'package:product_service_app_admin/ui/view/ap_button.dart';
import 'package:product_service_app_admin/util/base.dart';
import 'package:product_service_app_admin/util/validator.dart';

class AuthScene extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _AuthSceneState();
}

class _AuthSceneState extends State<AuthScene> with SHolder {
  final logic = Di.instance.loginLogic();
  _Mode mode;
  final formKey = GlobalKey<FormState>();
  final mailCtrl = TextEditingController();
  final passCtrl = TextEditingController();

  onLogin() {
    if (formKey.currentState.validate()) {
      logic.login(mailCtrl.text, passCtrl.text);
    }
  }

  @override
  void dispose() {
    super.dispose();
    logic.close();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Form(
          key: formKey,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              TextFormField(
                controller: mailCtrl,
                validator: validator(s),
              ),
              TextFormField(
                controller: passCtrl,
                validator: validator(s),
              ),
              APButton(
                Text("Войти"),
                onLogin,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

enum _Mode { Login, CreateAccount }
