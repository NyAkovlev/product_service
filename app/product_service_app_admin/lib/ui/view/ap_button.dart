import 'package:flutter/material.dart';

class APButton extends StatelessWidget {
  final Widget child;
  final VoidCallback onPressed;

  const APButton(this.child, this.onPressed, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      child: child,
      onPressed: onPressed,
    );
  }
}
