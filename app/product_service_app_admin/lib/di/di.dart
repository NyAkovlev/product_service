import 'package:dm/dm.dart';
import 'package:product_service_app_admin/ui/navigator/app_navigator.dart';
import 'package:product_service_app_admin/ui/scene/auth/logic/auth_logic.dart';

import 'di.dm.dart';
import 'module/api_module.dart';
import 'module/app_module.dart';
import 'module/network_module.dart';

@Modules([ApiModule, NetworkModule, AppModule])
abstract class Di {
  static Di instance;

  static Future init() async {
    instance = await DiImpl().init();
  }

  @provide
  AppNavigator appNavigator();

  AuthLogic loginLogic();
}
