import 'package:dm/dm.dart';
import 'package:domain_app/domain_app.dart';
import 'package:domain_app_impl/domain_app_impl.dart';
import 'package:product_service_app_admin/config.dart';
import 'package:shared_preferences/shared_preferences.dart';

class StorageModule {
  @provide
  @singleton
  Future<SharedPreferences> sharedPreferences() =>
      SharedPreferences.getInstance();

  @provide
  @singleton
  UserStorage userStorage(SharedPreferences sharedPreference) =>
      UserStorageImpl(
        sharedPreference,
        CLIENT_ID,
        CLIENT_SECRET,
      );
}
