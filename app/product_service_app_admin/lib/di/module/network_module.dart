import 'package:dm/dm.dart';
import 'package:domain_app_impl/domain_app_impl.dart';
import 'package:product_service_app_admin/config.dart';

class NetworkModule {
  @provide
  @singleton
  NetworkClient networkClient() => NetworkClient.url(HOST_URL);
}
