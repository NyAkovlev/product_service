import 'package:dm/dm.dart';
import 'package:domain_api/domain_api.dart';
import 'package:domain_app_impl/domain_app_impl.dart';

class ApiModule {
  @provide
  @singleton
  AuthApi authApi(NetworkClient client) => AuthApiRouteClient(client.post);

  @provide
  @singleton
  UserApi userApi(NetworkClient client) => UserApiRouteClient(client.post);

  @provide
  @singleton
  ProductApi productApi(NetworkClient client) =>
      ProductApiRouteClient(client.post);

  @provide
  @singleton
  CategoryApi categoryApi(NetworkClient client) =>
      CategoryApiRouteClient(client.post);
}
