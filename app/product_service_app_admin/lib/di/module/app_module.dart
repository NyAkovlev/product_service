import 'package:dm/dm.dart';
import 'package:product_service_app_admin/ui/navigator/app_navigator.dart';

class AppModule {
  @provide
  @singleton
  AppNavigator appNavigator() => AppNavigator();
}
