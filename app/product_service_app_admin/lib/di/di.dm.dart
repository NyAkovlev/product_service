// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// ManagerGenerator
// **************************************************************************

import 'package:product_service_app_admin/di/module/api_module.dart';
import 'package:product_service_app_admin/di/module/network_module.dart';
import 'package:product_service_app_admin/di/module/app_module.dart';
import 'package:product_service_app_admin/di/di.dart';
import 'package:domain_api/src/module/auth/auth_api.dart';
import 'package:domain_api/src/module/user/user_api.dart';
import 'package:domain_api/src/module/user/user.dart';
import 'package:domain_api/src/module/product/product_api.dart';
import 'package:domain_api/src/module/product/product.dart';
import 'package:domain_api/src/module/category/category.dart';
import 'package:domain_api/src/module/category/category_api.dart';
import 'package:domain_app_impl/src/network/client/network_client.dart';
import 'package:product_service_app_admin/ui/navigator/app_navigator.dart';
import 'package:product_service_app_admin/ui/scene/auth/logic/auth_logic.dart';

class DiImpl extends Di {
  final ApiModule _apiModule;
  final NetworkModule _networkModule;
  final AppModule _appModule;

  DiImpl({
    ApiModule apiModule,
    NetworkModule networkModule,
    AppModule appModule,
  })  : this._apiModule = apiModule ?? ApiModule(),
        this._networkModule = networkModule ?? NetworkModule(),
        this._appModule = appModule ?? AppModule();

  AuthApi _authApi;
  UserApi<MUser> _userApiMUser;
  ProductApi<MProduct, MCategory> _productApiMProductMCategory;
  CategoryApi<MCategory> _categoryApiMCategory;
  NetworkClient _networkClient;
  AppNavigator _appNavigator;

  Future<Di> init() async {
    return this;
  }

  AuthApi get _getAuthApi => _authApi ??= _apiModule.authApi(_getNetworkClient);
  UserApi<MUser> get _getUserApiMUser =>
      _userApiMUser ??= _apiModule.userApi(_getNetworkClient);
  ProductApi<MProduct, MCategory> get _getProductApiMProductMCategory =>
      _productApiMProductMCategory ??= _apiModule.productApi(_getNetworkClient);
  CategoryApi<MCategory> get _getCategoryApiMCategory =>
      _categoryApiMCategory ??= _apiModule.categoryApi(_getNetworkClient);
  NetworkClient get _getNetworkClient =>
      _networkClient ??= _networkModule.networkClient();
  AppNavigator get _getAppNavigator =>
      _appNavigator ??= _appModule.appNavigator();

  AppNavigator appNavigator() => _getAppNavigator;
  AuthLogic loginLogic() => AuthLogic(
        _getAuthApi,
      );
}
