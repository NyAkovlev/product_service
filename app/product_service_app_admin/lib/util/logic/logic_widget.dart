import 'dart:async';

import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

class StreamWidget<S> extends StatefulWidget {
  final BehaviorSubject<S> stream;
  final Widget Function(BuildContext, S) builder;

  const StreamWidget({
    Key key,
    this.stream,
    this.builder,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _StreamWidgetState<S>();
}

class _StreamWidgetState<S> extends State<StreamWidget> {
  StreamSubscription<S> _subscription;
  S _summary;

  @override
  void initState() {
    super.initState();
    _summary = widget.stream.value;
    _subscribe();
  }

  @override
  void didUpdateWidget(StreamWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.stream != widget.stream) {
      if (_subscription != null) {
        _unsubscribe();
      }
      _subscribe();
    }
  }

  @override
  Widget build(BuildContext context) => widget.builder(context, _summary);

  @override
  void dispose() {
    _unsubscribe();
    super.dispose();
  }

  void _subscribe() {
    if (widget.stream != null) {
      _subscription = widget.stream.listen((data) {
        _summary = data;
        setState(() {});
      }, onError: (Object error) {
        setState(() {});
      }, onDone: () {
        setState(() {});
      });
    }
  }

  void _unsubscribe() {
    if (_subscription != null) {
      _subscription.cancel();
      _subscription = null;
    }
  }
}
