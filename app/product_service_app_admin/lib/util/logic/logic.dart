import 'dart:async';

import 'package:rxdart/rxdart.dart';

class Logic<S> extends Stream<S> implements Sink<S> {
  final subject = BehaviorSubject<S>();

  @override
  void add(S data) {
    subject.add(data);
  }

  @override
  close() {
    subject.close();
  }

  @override
  StreamSubscription<S> listen(
    void Function(S event) onData, {
    Function onError,
    void Function() onDone,
    bool cancelOnError,
  }) {
    return subject.listen(
      onData,
      onError: onError,
      onDone: onDone,
      cancelOnError: cancelOnError,
    );
  }

}
