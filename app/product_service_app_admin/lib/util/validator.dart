import 'package:product_service_app_admin/res/str/s.dart';

String Function(String value) validator(
  S s, [
  List<Check> check,
  String message,
]) {
  return (value) {
    for (var item in (check ?? <Check>[CheckEmpty()])) {
      final result = item.check(s, value);
      if (result != null) {
        return message ?? result;
      }
    }
    return null;
  };
}

abstract class Check {
  final String _message;

  const Check(this._message);

  String check(S s, String value);
}

class CheckEmpty extends Check {
  const CheckEmpty({
    String message,
  }) : super(message);

  String check(S s, String value) {
    if (value.isEmpty) {
      return _message ?? s.fieldIsEmpty;
    }
    return null;
  }
}

class CheckLength extends Check {
  final int min;
  final int max;

  const CheckLength({
    this.min,
    this.max,
    String message,
  }) : super(message);

  String check(S s, String value) {
    final length = value.length;
    if ((max != null && length > max) || (min != null && length < min)) {
      if (max == null || min == null) {
        if (max == null) {
          return _message ?? s.invalidRangeMin(min);
        } else {
          return _message ?? s.invalidRangeMax(max);
        }
      } else {
        return _message ?? s.invalidRangeMinMax(min, max);
      }
    }
    return null;
  }
}
