import 'package:flutter/widgets.dart';
import 'package:product_service_app_admin/res/str/s.dart';
import 'package:provider/provider.dart';

mixin SHolder<W extends StatefulWidget> on State<W> {
  S s;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    s = Provider.of<S>(context);
  }

  @override
  void dispose() {
    super.dispose();
  }
}
