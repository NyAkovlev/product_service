import 'package:domain_api/domain_api.dart';
import 'package:json_annotation/json_annotation.dart';

part 'category.g.dart';

@JsonSerializable()
class Category extends MCategory {
  @override
  final int id;

  @override
  final String name;

  Category(this.id, this.name);

  Map<String, dynamic> toJson() => _$CategoryToJson(this);

  factory Category.fromJson(Map<String, dynamic> map) =>
      _$CategoryFromJson(map);
}
