import 'package:domain_api/domain_api.dart';
import 'package:domain_app/domain_app.dart';
import 'package:json_annotation/json_annotation.dart';

import 'category.dart';

part 'product_category.g.dart';

@JsonSerializable()
class ProductCategory extends MProductCategory {
  @override
  final String id;

  @override
  final Category category;

  @override
  final Product product;

  ProductCategory(this.category, this.product, this.id);

  Map<String, dynamic> toJson() => _$ProductCategoryToJson(this);

  factory ProductCategory.fromJson(Map<String, dynamic> map) =>
      _$ProductCategoryFromJson(map);
}
