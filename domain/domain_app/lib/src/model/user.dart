import 'package:domain_api/domain_api.dart';
import 'package:json_annotation/json_annotation.dart';
part 'user.g.dart';
@JsonSerializable()
class User extends MUser {
  @override
  final String email;

  @override
  final String password;

  @override
  final String phone;

  @override
  final int role;

  @override
  final String username;

  User(this.email, this.password, this.phone, this.role, this.username);

  Map<String, dynamic> toJson() => _$UserToJson(this);

  factory User.fromJson(Map<String, dynamic> map) =>
      _$UserFromJson(map);
}
