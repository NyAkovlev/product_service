import 'package:domain_api/domain_api.dart';
import 'package:json_annotation/json_annotation.dart';

part 'product.g.dart';

@JsonSerializable()
class Product extends MProduct {
  @override
  final int id;

  @override
  final String name;

  Product(this.id, this.name);

  Map<String, dynamic> toJson() => _$ProductToJson(this);

  factory Product.fromJson(Map<String, dynamic> map) => _$ProductFromJson(map);
}
