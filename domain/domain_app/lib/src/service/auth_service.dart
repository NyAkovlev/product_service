import 'package:domain_api/domain_api.dart';
import 'package:domain_app/domain_app.dart';

class AuthService {
  final AuthApi authApi;
  final UserStorage userStorage;

  AuthService(this.authApi, this.userStorage);

  Future login(String email, String pass) async {
    final response = await authApi.token(AuthRequest.login(
      email,
      pass,
      userStorage.clientId.get(),
      userStorage.clientSecret.get(),
    ));
    await _set(response);
  }

  Future refresh() async {
    final response = await authApi.token(AuthRequest.token(
      userStorage.refreshToken.get(),
      userStorage.clientId.get(),
      userStorage.clientSecret.get(),
    ));
    await _set(response);
  }

  Future _set(AuthResponse response) async {
    await userStorage.refreshToken.set(response.refreshToken);
    await userStorage.token.set(response.accessToken);
  }
}
