abstract class Preference<T> {
  final String key;

  Preference(this.key);

  T get();

  Future set(T value);
}
