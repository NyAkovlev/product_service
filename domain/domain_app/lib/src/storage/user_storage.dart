import 'package:domain_app/src/storage/preference.dart';

abstract class UserStorage {
  Preference<String> get token;

  Preference<String> get refreshToken;

  Preference<String> get clientId;

  Preference<String> get clientSecret;
}
