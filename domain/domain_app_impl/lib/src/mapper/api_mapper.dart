import 'package:domain_api/domain_api.dart';
import 'package:domain_app/domain_app.dart';

class ApiMapper extends ModelMap {
  @override
  AuthRequest authRequestFromJson(Map<String, dynamic> map) =>
      AuthRequest.fromJson(map);

  @override
  Map<String, dynamic> authRequestToJson(AuthRequest model) => model.toJson();

  @override
  AuthResponse authResponseFromJson(Map<String, dynamic> map) =>
      AuthResponse.fromJson(map);

  @override
  Map<String, dynamic> authResponseToJson(AuthResponse model) => model.toJson();

  @override
  MCategory categoryFromJson(Map<String, dynamic> map) =>
      Category.fromJson(map);

  @override
  Map<String, dynamic> categoryToJson(MCategory model) =>
      (model as Category).toJson();

  @override
  MProductCategory mProductCategoryFromJson(Map<String, dynamic> map) =>
      ProductCategory.fromJson(map);

  @override
  Map<String, dynamic> mProductCategoryToJson(MProductCategory model) =>
      (model as ProductCategory).toJson();

  @override
  MProduct productFromJson(Map<String, dynamic> map) => Product.fromJson(map);

  @override
  Map<String, dynamic> productToJson(MProduct model) =>
      (model as Product).toJson();

  @override
  MUser userFromJson(Map<String, dynamic> map) => User.fromJson(map);

  @override
  Map<String, dynamic> userToJson(MUser user) => (user as User).toJson();
}
