import 'package:dio/dio.dart';
import 'package:domain_api/domain_api.dart';
import 'package:domain_app_impl/src/network/interceptor/auth_interceptor.dart';

class NetworkClient {
  final Dio _dio;

  NetworkClient(this._dio);

  NetworkClient.url(String domain) : _dio = Dio(BaseOptions(baseUrl: domain));

  void initInterceptors(AuthInterceptor authInterceptor) {
    _dio.interceptors.add(authInterceptor);
  }

  Future post(ApiRequest request) async {
    return _dio.post("", data: request.toJson());
  }
}
