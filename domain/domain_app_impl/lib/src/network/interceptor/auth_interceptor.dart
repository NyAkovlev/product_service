import 'package:dio/dio.dart';
import 'package:domain_api/domain_api.dart';
import 'package:domain_app/domain_app.dart';

class AuthInterceptor extends Interceptor {
  final UserStorage userStorage;
  final AuthService authApi;
  final Dio dio;

  AuthInterceptor(this.userStorage, this.authApi, this.dio);

  @override
  Future onRequest(RequestOptions options) {
    _addToken(options.headers);
    return super.onRequest(options);
  }

  @override
  Future onResponse(Response _response) async {
    Response response = _response;
    final request = response.request;

    final code =response.statusCode;
    if (code == 401) {
      dio.lock();
      final result = await authApi.token(AuthRequest.token(
        userStorage.refreshToken.get(),
        userStorage.clientId.get(),
        userStorage.clientSecret.get(),
      ));
      await userStorage.refreshToken.set(result.refreshToken);
      await userStorage.token.set(result.accessToken);
      dio.unlock();


      _addToken(request.headers);
      response = await dio.request(
        request.path,
        queryParameters: request.queryParameters,
        cancelToken: request.cancelToken,
        onSendProgress: request.onSendProgress,
        onReceiveProgress: request.onReceiveProgress,
        data: request.data,
        options: request,
      );
    }

    return super.onResponse(response);
  }

  _addToken(Map<String, dynamic> headers) {
    headers["Authorization"] = "Bearer ${userStorage.token.get()}";
  }
}
