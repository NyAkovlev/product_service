import 'package:domain_app/domain_app.dart';
import 'package:domain_app_impl/src/storage/preference_impl.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserStorageImpl extends UserStorage {
  @override
  final Preference<String> refreshToken;

  @override
  final Preference<String> token;

  @override
  final Preference<String> clientId;

  @override
  final Preference<String> clientSecret;

  UserStorageImpl(
      SharedPreferences sharedPreferences, String clientId, String clientSecret)
      : refreshToken = StringPreference(sharedPreferences, "refreshToken"),
        token = StringPreference(sharedPreferences, "token"),
        clientId = ConstPreference(clientId),
        clientSecret = ConstPreference(clientSecret);
}
