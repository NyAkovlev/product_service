import 'package:domain_app/domain_app.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class _Preference<T> extends Preference<T> {
  final SharedPreferences sharedPreferences;

  _Preference(this.sharedPreferences, String key) : super(key);
}

class StringPreference extends _Preference<String> {
  StringPreference(SharedPreferences sharedPreferences, String key)
      : super(sharedPreferences, key);

  @override
  String get() {
    return sharedPreferences.get(key);
  }

  @override
  Future set(String value) {
    return sharedPreferences.setString(key, value);
  }
}

class ConstPreference<T> extends Preference<T> {
  final T value;

  ConstPreference(this.value) : super(null);

  @override
  T get() => value;

  @override
  Future set(T value) async => null;
}
