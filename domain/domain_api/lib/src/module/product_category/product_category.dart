import 'package:domain_api/domain_api.dart';

abstract class MProductCategory {
  String get id;

  MProduct get product;

  MCategory get category;
}
