import 'package:domain_api/domain_api.dart';

abstract class ProductApi<P extends MProduct,C extends MCategory> {
  Future delete(P product);

  Future add(P product);

  Future edit(P product);

  Future<List<P>> getAll();

  Future<P> addCategory(P product, C category);
}
