import 'package:domain_api/domain_api.dart';

abstract class MProduct {
  int get id;

  String get name;

}
