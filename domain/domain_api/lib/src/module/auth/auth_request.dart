import 'package:json_annotation/json_annotation.dart';

part 'auth_request.g.dart';

@JsonSerializable()
class AuthRequest {
  final String username;
  final String password;
  final String refreshToken;
  final String clientId;
  final String clientSecret;
  final String grantType;

  AuthRequest(
    this.username,
    this.password,
    this.refreshToken,
    this.clientId,
    this.clientSecret,
    this.grantType,
  );

  AuthRequest.token(
    this.refreshToken,
    this.clientId,
    this.clientSecret,
  )   : username = null,
        password = null,
        grantType = TYPE_REFRESH_TOKEN;

  AuthRequest.login(
    this.username,
    this.password,
    this.clientId,
    this.clientSecret,
  )   : refreshToken = null,
        grantType = TYPE_PASSWORD;

  Map<String, dynamic> toJson() => _$AuthRequestToJson(this);

  factory AuthRequest.fromJson(Map<String, dynamic> map) =>
      _$AuthRequestFromJson(map);
  static const TYPE_PASSWORD = "password";
  static const TYPE_REFRESH_TOKEN = "refresh_token";
}
