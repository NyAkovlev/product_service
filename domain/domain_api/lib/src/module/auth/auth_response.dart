import 'package:json_annotation/json_annotation.dart';

part 'auth_response.g.dart';

@JsonSerializable()
class AuthResponse {
  final String accessToken;
  final String refreshToken;
  final int expiresIn;

  AuthResponse({
    this.accessToken,
    this.refreshToken,
    this.expiresIn,
  });

  Map<String, dynamic> toJson() => _$AuthResponseToJson(this);

  factory AuthResponse.fromJson(Map<String, dynamic> map) =>
      _$AuthResponseFromJson(map);
}
