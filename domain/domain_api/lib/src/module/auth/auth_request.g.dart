// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'auth_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AuthRequest _$AuthRequestFromJson(Map<String, dynamic> json) {
  return AuthRequest(
    json['username'] as String,
    json['password'] as String,
    json['refreshToken'] as String,
    json['clientId'] as String,
    json['clientSecret'] as String,
    json['grantType'] as String,
  );
}

Map<String, dynamic> _$AuthRequestToJson(AuthRequest instance) =>
    <String, dynamic>{
      'username': instance.username,
      'password': instance.password,
      'refreshToken': instance.refreshToken,
      'clientId': instance.clientId,
      'clientSecret': instance.clientSecret,
      'grantType': instance.grantType,
    };
