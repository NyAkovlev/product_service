import 'package:domain_api/domain_api.dart';

abstract class AuthApi {
  Future<AuthResponse> token(AuthRequest model);
}
