abstract class MUser {
  String get phone;

  String get email;

  String get password;

  String get username;

  int get role;
}

class Roles {
  static const ADMIN = 0;
  static const DEFAULT = 9999;
}
