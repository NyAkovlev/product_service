import 'package:domain_api/src/module/user/user.dart';

abstract class UserApi<U extends MUser>{
  Future<U> add(U user);

  Future<U> edit(U product);
}
