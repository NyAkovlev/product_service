import 'category.dart';

abstract class CategoryApi<C extends MCategory> {
  Future delete(C category);

  Future add(C category);

  Future edit(C category);

  Future<List<C>> getAll();
}
