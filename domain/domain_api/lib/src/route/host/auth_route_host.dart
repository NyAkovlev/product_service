import 'package:domain_api/domain_api.dart';
import 'package:domain_api/src/route/base/auth_route.dart';
import 'package:domain_api/src/route/route.dart';

class AuthRouteHost extends AuthRoute with RouteHost {
  final AuthApi api;

  AuthRouteHost(this.api);

  Future<dynamic> requestMap(AuthMethod method, dynamic arg) {
    switch (method) {
      case AuthMethod.token:
        return api
            .token(modelMap.authRequestFromJson(arg))
            .then((item) => modelMap.authResponseToJson(item));
        break;
      default:
        return null;
    }
  }
}
