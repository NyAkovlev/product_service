import 'package:domain_api/domain_api.dart';
import 'package:domain_api/src/route/base/user_route.dart';
import 'package:domain_api/src/route/route.dart';

class UserRouteHost extends UserRoute with RouteHost {
  final UserApi api;

  UserRouteHost(this.api);

  Future<dynamic> requestMap(UserMethod method, dynamic arg) {
    switch (method) {
      case UserMethod.add:
        return api
            .add(modelMap.userFromJson(arg))
            .then((item) => modelMap.userToJson(item));
        break;
      case UserMethod.edit:
        return api
            .edit(modelMap.userFromJson(arg))
            .then((item) => modelMap.userToJson(item));
        break;
      default:
        return null;
    }
  }
}
