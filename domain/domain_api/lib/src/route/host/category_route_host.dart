import 'package:domain_api/domain_api.dart';
import 'package:domain_api/src/route/base/category_route.dart';
import 'package:domain_api/src/route/route.dart';

class CategoryRouteHost extends CategoryRoute with RouteHost {
  final CategoryApi api;

  CategoryRouteHost(this.api);

  Future<dynamic> requestMap(CategoryMethod method, dynamic arg) {
    switch (method) {
      case CategoryMethod.delete:
        return api.delete(modelMap.categoryFromJson(arg));
      case CategoryMethod.add:
        return api.add(modelMap.categoryFromJson(arg));
      case CategoryMethod.getAll:
        return api.getAll();
      case CategoryMethod.edit:
        return api.delete(modelMap.categoryFromJson(arg));
      default:
        return null;
    }
  }
}
