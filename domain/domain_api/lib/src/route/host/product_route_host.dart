import 'package:domain_api/domain_api.dart';
import 'package:domain_api/src/route/base/product_route.dart';

class ProductRouteHost extends ProductRoute with RouteHost {
  final ProductApi api;

  ProductRouteHost(this.api);

  @override
  Future requestMap(ProductMethod method, dynamic arg) {
    switch (method) {
      case ProductMethod.delete:
        return api.delete(modelMap.productFromJson(arg));
      case ProductMethod.add:
        return api.add(modelMap.productFromJson(arg));
      case ProductMethod.edit:
        return api.edit(modelMap.productFromJson(arg));
      case ProductMethod.getAll:
        return api.getAll();
      case ProductMethod.addCategory:
        return api.addCategory(
          modelMap.productFromJson(arg["product"]),
          modelMap.categoryFromJson(arg["category"]),
        );
      default:
        return null;
    }
  }
}
