import 'package:domain_api/domain_api.dart';

abstract class Route<Enum> {
  String get module;

  String toKey(Enum value);

  Enum fromKey(String value);
}

mixin RouteClient<Enum> on Route<Enum> {
  Future Function(ApiRequest) get onRequest;

  Future request(Enum method, dynamic arg) {
    return onRequest(ApiRequest(module, toKey(method), arg));
  }
}

mixin RouteHost<Enum, Model> on Route<Enum> {
  Future<dynamic> requestMap(Enum method, dynamic arg);
}

class RouteMap {
  final Map<String, RouteHost> _routes;

  RouteMap(List<RouteHost> routes) : _routes = _routeMap(routes);

  Future<dynamic> map(ApiRequest request) {
    final route = _routes[request.module];
    if (route == null) {
      throw ModuleNotFound(request.module);
    }
    final method = route.fromKey(request.method);
    final future = route.requestMap(method, request.arg);
    if (future == null) {
      throw MethodNotFound(request.module, method.toString());
    }
    return future;
  }

  static Map<String, RouteHost> _routeMap(List<RouteHost> routes) {
    final map = <String, RouteHost>{};
    for (var route in routes) {
      map[route.module] = route;
    }
    return map;
  }
}
