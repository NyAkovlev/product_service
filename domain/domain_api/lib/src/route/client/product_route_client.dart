import 'package:domain_api/domain_api.dart';
import 'package:domain_api/src/route/base/product_route.dart';

class ProductApiRouteClient extends ProductRoute
    with RouteClient
    implements ProductApi {
  @override
  final Future Function(ApiRequest) onRequest;

  ProductApiRouteClient(this.onRequest);

  @override
  Future add(MProduct product) {
    return request(ProductMethod.add, modelMap.productToJson(product));
  }

  @override
  Future delete(MProduct product) {
    return request(ProductMethod.delete, modelMap.productToJson(product));
  }

  @override
  Future edit(MProduct product) {
    return request(ProductMethod.edit, modelMap.productToJson(product));
  }

  @override
  Future<List<MProduct>> getAll() {
    return request(ProductMethod.getAll, null);
  }

  @override
  Future<MProduct> addCategory(MProduct product, MCategory category) {
    return request(
      ProductMethod.getAll,
      {
        "product": product,
        "category": category,
      },
    );
  }
}
