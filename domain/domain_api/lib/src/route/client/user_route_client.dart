import 'package:domain_api/domain_api.dart';
import 'package:domain_api/src/route/base/user_route.dart';

class UserApiRouteClient extends UserRoute with RouteClient implements UserApi {
  @override
  final Future Function(ApiRequest) onRequest;

  UserApiRouteClient(this.onRequest);

  @override
  Future<MUser> add(MUser user) {
    return request(UserMethod.add, modelMap.userToJson(user))
        .then((map) => modelMap.userFromJson(map));
  }

  @override
  Future<MUser> edit(MUser user) {
    return request(UserMethod.add, modelMap.userToJson(user))
        .then((map) => modelMap.userFromJson(map));
  }
}
