import 'package:domain_api/domain_api.dart';
import 'package:domain_api/src/route/base/auth_route.dart';
 class AuthApiRouteClient extends AuthRoute
    with RouteClient
    implements AuthApi {
  @override
  final Future Function(ApiRequest) onRequest;

  AuthApiRouteClient(this.onRequest);

  @override
  Future<AuthResponse> token(AuthRequest model) {
    return request(AuthMethod.token, modelMap.authRequestToJson(model))
        .then((map) => modelMap.authResponseFromJson(map));
  }
}
