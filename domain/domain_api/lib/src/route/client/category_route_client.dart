import 'package:domain_api/domain_api.dart';
import 'package:domain_api/src/route/base/category_route.dart';

class CategoryApiRouteClient extends CategoryRoute
    with RouteClient
    implements CategoryApi {
  @override
  final Future<Map<String, dynamic>> Function(ApiRequest) onRequest;

  CategoryApiRouteClient(this.onRequest);

  @override
  Future add(MCategory category) {
    return request(CategoryMethod.add, modelMap.categoryToJson(category));
  }

  @override
  Future delete(MCategory category) {
    return request(CategoryMethod.delete, modelMap.categoryToJson(category));
  }

  @override
  Future edit(MCategory category) {
    return request(CategoryMethod.edit, modelMap.categoryToJson(category));
  }

  @override
  Future<List<MCategory>> getAll() {
    return request(CategoryMethod.getAll, null).then((map) =>
        (map as List).map((item) => modelMap.categoryFromJson(item)).toList());
  }
}
