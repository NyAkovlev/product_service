export 'route.dart';

export 'base/category_route.dart';
export 'base/product_route.dart';
export 'base/auth_route.dart';
export 'base/user_route.dart';

//client
export 'client/category_route_client.dart';
export 'client/product_route_client.dart';
export 'client/auth_route_client.dart';
export 'client/user_route_client.dart';

//host
export 'host/category_route_host.dart';
export 'host/product_route_host.dart';
export 'host/auth_route_host.dart';
export 'host/user_route_host.dart';
