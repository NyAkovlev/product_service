import 'package:domain_api/domain_api.dart';

abstract class ProductRoute extends Route<ProductMethod> {
  @override
  final module = "product";

  @override
  ProductMethod fromKey(String value) => ProductMethodMap.fromKey(value);

  @override
  String toKey(ProductMethod value) => ProductMethodMap.toKey(value);
}

enum ProductMethod {
  delete,
  add,
  getAll,
  edit,
  addCategory,
}

class ProductMethodMap {
  static ProductMethod fromKey(String value) {
    switch (value) {
      case "delete":
        return ProductMethod.delete;
      case "add":
        return ProductMethod.add;
      case "getAll":
        return ProductMethod.getAll;
      case "edit":
        return ProductMethod.edit;
      case "addCategory":
        return ProductMethod.addCategory;
    }
    return null;
  }

  static String toKey(ProductMethod value) {
    switch (value) {
      case ProductMethod.delete:
        return "delete";
      case ProductMethod.add:
        return "add";
      case ProductMethod.getAll:
        return "getAll";
      case ProductMethod.edit:
        return "edit";
      case ProductMethod.addCategory:
        return "addCategory";
    }
    return null;
  }
}
