import 'package:domain_api/domain_api.dart';
import 'package:domain_api/src/route/route.dart';

abstract class UserRoute extends Route<UserMethod> {
  @override
  final module = "user";

  @override
  UserMethod fromKey(String value) => UserMethodMap.fromKey(value);

  @override
  String toKey(UserMethod value) => UserMethodMap.toKey(value);
}

enum UserMethod {
  add,
  edit,
}

class UserMethodMap {
  static UserMethod fromKey(String value) {
    switch (value) {
      case "add":
        return UserMethod.add;
      case "edit":
        return UserMethod.edit;
    }
    return null;
  }

  static String toKey(UserMethod value) {
    switch (value) {
      case UserMethod.edit:
        return "edit";
      case UserMethod.add:
        return "add";
    }
    return null;
  }
}
