import 'package:domain_api/domain_api.dart';
import 'package:domain_api/src/route/route.dart';

abstract class AuthRoute extends Route<AuthMethod> {
  @override
  final module = "auth";

  @override
  AuthMethod fromKey(String value) => AuthMethodMap.fromKey(value);

  @override
  String toKey(AuthMethod value) => AuthMethodMap.toKey(value);
}

enum AuthMethod {
  token,
}

class AuthMethodMap {
  static AuthMethod fromKey(String value) {
    switch (value) {
      case "token":
        return AuthMethod.token;
    }
    return null;
  }

  static String toKey(AuthMethod value) {
    switch (value) {
      case AuthMethod.token:
        return "token";
    }
    return null;
  }
}
