import 'package:domain_api/domain_api.dart';
import 'package:domain_api/src/route/route.dart';

abstract class CategoryRoute extends Route<CategoryMethod> {
  @override
  final module = "category";

  @override
  CategoryMethod fromKey(String value) => CategoryMethodMap.fromKey(value);

  @override
  String toKey(CategoryMethod value) => CategoryMethodMap.toKey(value);
}

enum CategoryMethod {
  delete,
  add,
  getAll,
  edit,
}

class CategoryMethodMap {
  static CategoryMethod fromKey(String value) {
    switch (value) {
      case "delete":
        return CategoryMethod.delete;
      case "add":
        return CategoryMethod.add;
      case "getAll":
        return CategoryMethod.getAll;
      case "edit":
        return CategoryMethod.edit;
    }
    return null;
  }

  static String toKey(CategoryMethod value) {
    switch (value) {
      case CategoryMethod.delete:
        return "delete";
      case CategoryMethod.add:
        return "add";
      case CategoryMethod.getAll:
        return "getAll";
      case CategoryMethod.edit:
        return "edit";
    }
    return null;
  }
}
