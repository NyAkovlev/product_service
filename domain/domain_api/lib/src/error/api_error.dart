import 'package:like_sealed/like_sealed.dart';

@likeSealed
abstract class ApiError {
  final int code;
  final String error;
  final String description;

  ApiError(this.error, this.description, this.code);

  Map<String, dynamic> toJson() => {
        "class": runtimeType.toString(),
        "code": code,
        "error": error,
        "description": description,
      };
}

class UndefinedError extends ApiError {
  UndefinedError() : super(null, null, 500);
}

class ModuleNotFound extends ApiError {
  ModuleNotFound(String module)
      : super("module not found", "module $module not found", 404);
}

class MethodNotFound extends ApiError {
  MethodNotFound(String module, String method)
      : super("method not found", "method $method in module $module not found",
            404);
}

class Forbidden extends ApiError {
  Forbidden(String description)
      : super(
          "Forbidden",
          description,
          403,
        );
}

class InvalidRequest extends ApiError {
  InvalidRequest(String message, String description)
      : super(
          message,
          description,
          400,
        );
}

class DuplicateUnique extends InvalidRequest {
  DuplicateUnique(String description)
      : super(
          "Duplicate unique value",
          description,
        );
}

class InvalidConstantValue extends InvalidRequest {
  InvalidConstantValue(String field, String value, List<String> values)
      : super(
          "invalid constant value",
          "invalid constant value '$value' in field '$field'. You can use '${values.join("','")}'",
        );
}

class InvalidArgument extends InvalidRequest {
  InvalidArgument(String field, String value)
      : super(
          "Invalid argument",
          "Invalid argument '$value' in field '$field}'",
        );
}
