// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// SealedGenerator
// **************************************************************************

import 'package:domain_api/src/error/api_error.dart';
import 'package:like_sealed/sealed_switch.dart';

class ApiErrors {
  static UndefinedError undefinedError() {
    return UndefinedError();
  }

  static ModuleNotFound moduleNotFound(String module) {
    return ModuleNotFound(module);
  }

  static MethodNotFound methodNotFound(String module, String method) {
    return MethodNotFound(module, method);
  }

  static Forbidden forbidden(String description) {
    return Forbidden(description);
  }

  static InvalidRequest invalidRequest(String message, String description) {
    return InvalidRequest(message, description);
  }

  static DuplicateUnique duplicateUnique(String description) {
    return DuplicateUnique(description);
  }

  static InvalidConstantValue invalidConstantValue(
      String field, String value, List<String> values) {
    return InvalidConstantValue(field, value, values);
  }

  static InvalidArgument invalidArgument(String field, String value) {
    return InvalidArgument(field, value);
  }
}

abstract class ApiErrorSwitch<T> implements SealedSwitch<ApiError, T> {
  T switchCase(ApiError type) => type is UndefinedError
      ? onUndefinedError(type)
      : type is ModuleNotFound
          ? onModuleNotFound(type)
          : type is MethodNotFound
              ? onMethodNotFound(type)
              : type is Forbidden
                  ? onForbidden(type)
                  : type is InvalidRequest
                      ? onInvalidRequest(type)
                      : type is DuplicateUnique
                          ? onDuplicateUnique(type)
                          : type is InvalidConstantValue
                              ? onInvalidConstantValue(type)
                              : type is InvalidArgument
                                  ? onInvalidArgument(type)
                                  : type is ApiError ? onDefault(type) : null;
  T onUndefinedError(UndefinedError undefinedError);
  T onModuleNotFound(ModuleNotFound moduleNotFound);
  T onMethodNotFound(MethodNotFound methodNotFound);
  T onForbidden(Forbidden forbidden);
  T onInvalidRequest(InvalidRequest invalidRequest);
  T onDuplicateUnique(DuplicateUnique duplicateUnique);
  T onInvalidConstantValue(InvalidConstantValue invalidConstantValue);
  T onInvalidArgument(InvalidArgument invalidArgument);
  T onDefault(ApiError apiError);
}
