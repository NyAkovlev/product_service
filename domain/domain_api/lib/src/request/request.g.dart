// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ApiRequest _$ApiRequestFromJson(Map<String, dynamic> json) {
  return ApiRequest(
    json['module'] as String,
    json['method'] as String,
    json['arg'],
  );
}

Map<String, dynamic> _$ApiRequestToJson(ApiRequest instance) =>
    <String, dynamic>{
      'module': instance.module,
      'method': instance.method,
      'arg': instance.arg,
    };
