import 'package:domain_api/domain_api.dart';

ModelMap modelMap;

abstract class ModelMap {
  Map<String, dynamic> categoryToJson(MCategory model);

  MCategory categoryFromJson(Map<String, dynamic> map);

  Map<String, dynamic> productToJson(MProduct model);

  MProduct productFromJson(Map<String, dynamic> map);

  Map<String, dynamic> mProductCategoryToJson(MProductCategory model);

  MProductCategory mProductCategoryFromJson(Map<String, dynamic> map);

  Map<String, dynamic> authRequestToJson(AuthRequest model);

  AuthRequest authRequestFromJson(Map<String, dynamic> map);

  Map<String, dynamic> authResponseToJson(AuthResponse model);

  AuthResponse authResponseFromJson(Map<String, dynamic> map);

  Map<String, dynamic> userToJson(MUser user);

  MUser userFromJson(Map<String, dynamic> map);
}
