import 'package:aqueduct/managed_auth.dart';
import 'package:domain_api/domain_api.dart';
import 'package:product_service_server/auth/user_provider.dart';
import 'package:product_service_server/db/api/category_api_impl.dart';
import 'package:product_service_server/db/api/product_api_impl.dart';
import 'package:product_service_server/db/api/user_api_impl.dart';
import 'package:product_service_server/db/model/product.dart';
import 'package:product_service_server/db/model/product_category_db.dart';
import 'package:product_service_server/route/model_map.dart';

import 'configuration.dart';
import 'db/api/auth_api_impl.dart';
import 'db/model/category_db.dart';
import 'db/model/user.dart';
import 'product_service_server.dart';
import 'route/route_controller.dart';

class ProductServiceServerChannel extends ApplicationChannel {
  Config config;
  ManagedContext context;
  List<RouteHost> routes;
  AuthServer authServer;

  @override
  Future prepare() async {
    logger.onRecord.listen(
      (rec) => print("$rec ${rec.error ?? ""} ${rec.stackTrace ?? ""}"),
    );

    config = Config(options.configurationFilePath);

    final dataModel = ManagedDataModel.fromCurrentMirrorSystem();

    final persistentStore = PostgreSQLPersistentStore.fromConnectionInfo(
      config.database.username,
      config.database.password,
      config.database.host,
      config.database.port,
      config.database.databaseName,
    );

    context = ManagedContext(dataModel, persistentStore);

    final authDelegate = ManagedAuthDelegate<User>(
      context,
      tokenLimit: /*todo обсудить */ 10,
    );
    authServer = AuthServer(authDelegate);

    routes = [
      CategoryRouteHost(CategoryApiImpl(context)),
      ProductRouteHost(ProductApiImpl(context)),
      AuthRouteHost(AuthApiImpl(authServer, context)),
      UserRouteHost(UserApiImpl(authServer, context)),
    ];

    modelMap = ModelMapImpl();
  }

  @override
  Controller get entryPoint {
    final router = Router();

    router
        .route("/api")
        .link(() => UserProvider(authServer))
        .link(() => RouteController(routes,context));
    return router;
  }
}
