import 'dart:async';

class FutureProvider<T, V> implements Future<T> {
  Future<T> Function(V) _futureEmitter;
  Future<T> _future;

  Type get provideType => V;

  FutureProvider(this._futureEmitter) : assert(_futureEmitter != null);

  Future provide(V value) {
    assert(_futureEmitter != null);
    _future = _futureEmitter(value);
    _futureEmitter = null;
    return _future;
  }

  Future<T> _getFuture() {
    if (_future != null) {
      return _future;
    } else {
      provide(null);
      return _future;
    }
  }

  @override
  Stream<T> asStream() => _getFuture().asStream();

  @override
  Future<T> catchError(Function onError, {bool Function(Object error) test}) =>
      _getFuture().catchError(
        onError,
        test: test,
      );

  @override
  Future<S> then<S>(FutureOr<S> Function(T) onValue, {Function onError}) =>
      _getFuture().then(
        onValue,
        onError: onError,
      );

  @override
  Future<T> whenComplete(FutureOr Function() action) =>
      _getFuture().whenComplete(action);

  @override
  Future<T> timeout(Duration timeLimit, {FutureOr<T> Function() onTimeout}) =>
      _getFuture().timeout(
        timeLimit,
        onTimeout: onTimeout,
      );
}
