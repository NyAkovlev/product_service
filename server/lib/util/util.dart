T tryOr<T>(
  T Function() tryAction, [
  T Function() catchAction,
]) {
  try {
    final r = tryAction();
    return r;
  } catch (_) {
    if (catchAction == null) {
      return null;
    }
    return catchAction();
  }
}
