import 'package:domain_api/domain_api.dart';
import 'package:product_service_server/db/model/category_db.dart';
import 'package:product_service_server/db/model/product_category_db.dart';
import 'package:product_service_server/db/model/product.dart';
import 'package:product_service_server/db/model/user.dart';

class ModelMapImpl extends ModelMap {
  @override
  MCategory categoryFromJson(Map<String, dynamic> map) =>
      Category()..readFromMap(map);

  @override
  Map<String, dynamic> categoryToJson(MCategory model) =>
      (model as Category).asMap();

  @override
  MProductCategory mProductCategoryFromJson(Map<String, dynamic> map) =>
      ProductCategory()..readFromMap(map);

  @override
  Map<String, dynamic> mProductCategoryToJson(MProductCategory model) =>
      (model as ProductCategory).asMap();

  @override
  MProduct productFromJson(Map<String, dynamic> map) =>
      Product()..readFromMap(map);

  @override
  Map<String, dynamic> productToJson(MProduct model) =>
      (model as Product).asMap();

  @override
  AuthRequest authRequestFromJson(Map<String, dynamic> map) =>
      AuthRequest.fromJson(map);

  @override
  Map<String, dynamic> authRequestToJson(AuthRequest model) => model.toJson();

  @override
  AuthResponse authResponseFromJson(Map<String, dynamic> map) =>
      AuthResponse.fromJson(map);

  @override
  Map<String, dynamic> authResponseToJson(AuthResponse model) => model.toJson();

  @override
  MUser userFromJson(Map<String, dynamic> map) => User()..readFromMap(map);

  @override
  Map<String, dynamic> userToJson(MUser user) => (user as User).asMap();
}
