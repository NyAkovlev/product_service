import 'package:aqueduct/aqueduct.dart';
import 'package:product_service_server/db/model/user.dart';
import 'package:product_service_server/util/future_provider.dart';

class RouteProvider {
  final Request request;
  final ManagedContext context;
  User _user;

  RouteProvider(this.context, this.request);

  Future provide(Future future) async {
    Future current = future;
    while (current is FutureProvider) {
      final provider = (current as FutureProvider);
      final type = provider.provideType;
      current = provider.provide(_selectType(type));
    }
    return current;
  }

  Future _selectType(Type type) async {
    if (type is User) {
      return _getUser();
    } else {
      print("$type not provided");
      return null;
    }
  }

  Future<User> _getUser() async {
    return _user ??= await (Query<User>(context)
          ..where((u) => u.id).equalTo(request.authorization.ownerID))
        .fetchOne();
  }
}
