import 'package:domain_api/domain_api.dart';
import 'package:product_service_server/product_service_server.dart';
import 'package:product_service_server/route/route_provider.dart';

class RouteController extends Controller {
  final RouteMap routeMap;
  final ManagedContext context;

  RouteController(List<RouteHost> routes, this.context)
      : routeMap = RouteMap(routes);

  @override
  FutureOr<RequestOrResponse> handle(Request request) async {
    try {
      final map = await request.body.decode<Map<String, dynamic>>().catchError(
            (_) => throw ApiErrors.invalidRequest("cant parse request", null),
          );

      final apiResponse = ApiRequest.fromJson(map);
      Future future = routeMap.map(apiResponse);

      final provider = RouteProvider(context, request);

      final result = await provider.provide(future);

      return Response(200, {}, result);
    } catch (e, s) {
      return error(mapError(e, s));
    }
  }

  ApiError mapError(e, StackTrace s) {
    if (e is ApiError) {
      if (e is UndefinedError) {
        print(s);
      }
      return e;
    }
    if (e is QueryException) {
      if (e.event == QueryExceptionEvent.conflict) {
        return ApiErrors.duplicateUnique(e.message);
      }
    }

    if (e is ValidationException) {
      return ApiErrors.invalidRequest(
          "Validation exception", e.reasons.join("\n"));
    }
    print(s);
    return UndefinedError();
  }

  Response error(ApiError error) {
    return Response(error.code, {}, error.toJson());
  }
}
