import 'dart:io';

import 'package:aqueduct/aqueduct.dart';

class Config extends Configuration {
  DatabaseConfiguration database;
  String url;
  int port;

  @optionalConfiguration
  int identifier;

  Config(String path) : super.fromFile(File(path));
}
