import 'package:product_service_server/product_service_server.dart';

class UserProvider extends Controller {
  UserProvider(
    this.validator, {
    this.parser = const AuthorizationBearerParser(),
    List<String> scopes,
  }) : scopes = scopes?.map((s) => AuthScope(s))?.toList();

  final AuthValidator validator;

  final List<AuthScope> scopes;

  final AuthorizationParser parser;

  @override
  FutureOr<RequestOrResponse> handle(Request request) async {
    final authData = request.raw.headers.value(HttpHeaders.authorizationHeader);
    if (authData == null) {
      return request;
    }

    try {
      final value = parser.parse(authData);
      request.authorization =
          await validator.validate(parser, value, requiredScope: scopes);
      if (request.authorization == null) {
        return Response.unauthorized();
      }

      _addScopeRequirementModifier(request);
    } on AuthorizationParserException catch (e) {
      return _responseFromParseException(e);
    } on AuthServerException catch (e) {
      if (e.reason == AuthRequestError.invalidScope) {
        return Response.forbidden(body: {
          "error": "insufficient_scope",
          "scope": scopes.map((s) => s.toString()).join(" ")
        });
      }

      return Response.unauthorized();
    }

    return request;
  }

  Response _responseFromParseException(AuthorizationParserException e) {
    switch (e.reason) {
      case AuthorizationParserExceptionReason.malformed:
        return Response.badRequest(
            body: {"error": "invalid_authorization_header"});
      case AuthorizationParserExceptionReason.missing:
        return Response.unauthorized();
      default:
        return Response.serverError();
    }
  }

  void _addScopeRequirementModifier(Request request) {
    // If a controller returns a 403 because of invalid scope,
    // this Authorizer adds its required scope as well.
    if (scopes != null) {
      request.addResponseModifier((resp) {
        if (resp.statusCode == 403 && resp.body is Map) {
          final body = resp.body as Map<String, dynamic>;
          if (body.containsKey("scope")) {
            final declaredScopes = (body["scope"] as String).split(" ");
            final scopesToAdd = scopes
                .map((s) => s.toString())
                .where((s) => !declaredScopes.contains(s));
            body["scope"] =
                [scopesToAdd, declaredScopes].expand((i) => i).join(" ");
          }
        }
      });
    }
  }
}
