import 'package:aqueduct/aqueduct.dart';
import 'package:domain_api/domain_api.dart';
import 'package:product_service_server/db/model/user.dart';
import 'package:product_service_server/util/future_provider.dart';

class AuthApiImpl extends AuthApi {
  final AuthServer authServer;
  final ManagedContext context;

  AuthApiImpl(this.authServer, this.context);

  @override
  Future<AuthResponse> token(AuthRequest model) {
    return FutureProvider((User user) async {
      final grantType = model.grantType;
      if (grantType == AuthRequest.TYPE_PASSWORD) {
        final token = await authServer.authenticate(
          model.username,
          model.password,
          model.clientId,
          model.clientSecret,
          requestedScopes: [],
        );

        return AuthResponse(
          accessToken: token.accessToken,
          refreshToken: token.refreshToken,
          expiresIn:
              token.expirationDate.difference(DateTime.now().toUtc()).inSeconds,
        );
      } else if (grantType == AuthRequest.TYPE_REFRESH_TOKEN) {
        final token = await authServer.refresh(
          model.refreshToken,
          model.clientId,
          model.clientSecret,
          requestedScopes: [],
        );

        return AuthResponse(
          accessToken: token.accessToken,
          refreshToken: token.refreshToken,
          expiresIn:
              token.expirationDate.difference(DateTime.now().toUtc()).inSeconds,
        );
      } else {
        throw ApiErrors.invalidConstantValue(
          "grantType",
          grantType,
          [AuthRequest.TYPE_PASSWORD, AuthRequest.TYPE_REFRESH_TOKEN],
        );
      }
    });
  }
}
