import 'package:aqueduct/aqueduct.dart';
import 'package:domain_api/domain_api.dart';
import 'package:product_service_server/db/model/category_db.dart';
import 'package:product_service_server/util/future_provider.dart';

class CategoryApiImpl extends CategoryApi<Category> {
final  ManagedContext context;

  CategoryApiImpl(this.context);

  @override
  Future add(Category category) async {
    final model = await context.insertObject(category);
    return model;
  }

  @override
  Future delete(Category category) async {
    final query = Query<Category>(context)
      ..where((u) => u.id).equalTo(category.id);
    return query.delete();
  }

  @override
  Future edit(Category category) async {
    final query = Query<Category>(context)
      ..values.name = category.name
      ..where((u) => u.id).equalTo(category.id);

    return query.updateOne();
  }

  @override
  Future<List<Category>> getAll() {
    return FutureProvider((int user) {
      var query = Query<Category>(context);
      (query.join(set: (p) => p.products)
            ..returningProperties((c) => [c.id, c.product]))
          .join(object: (c) => c.product);

      return query.fetch();
    });
  }
}
