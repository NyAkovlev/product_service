import 'package:aqueduct/aqueduct.dart';
import 'package:domain_api/domain_api.dart';
import 'package:product_service_server/db/model/user.dart';
import 'package:product_service_server/util/future_provider.dart';

class UserApiImpl extends UserApi<User> {
  final AuthServer authServer;
  final ManagedContext context;

  UserApiImpl(this.authServer, this.context);

  @override
  Future<User> add(User item) {
    return FutureProvider((User current) async {
      final itemRole = item.role ??= Roles.DEFAULT;
      final currentRole = current?.role ?? Roles.DEFAULT;

      if (itemRole > currentRole) {
        throw ApiErrors.forbidden("You can create user with role ${item.role}");
      }

      item
        ..salt = AuthUtility.generateRandomSalt()
        ..hashedPassword = authServer.hashPassword(item.password, item.salt);

      return await Query<User>(context, values: item).insert();
    });
  }

  @override
  Future<User> edit(User item) {
    return FutureProvider((User current) async {
      final itemRole = item.role ??= Roles.DEFAULT;
      final currentRole = current?.role ?? Roles.DEFAULT;

      if (itemRole > currentRole) {
        throw ApiErrors.forbidden("You can create user with role ${item.role}");
      }

      final result = await Query<User>(context, values: item).updateOne();

      return result;
    });
  }
}
