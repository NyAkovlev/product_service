import 'package:aqueduct/aqueduct.dart';
import 'package:domain_api/domain_api.dart';
import 'package:product_service_server/db/model/category_db.dart';
import 'package:product_service_server/db/model/product_category_db.dart';
import 'package:product_service_server/db/model/product.dart';

class ProductApiImpl extends ProductApi<Product, Category> {
  ManagedContext context;

  ProductApiImpl(this.context);

  @override
  Future add(Product category) async {
    final model = await context.insertObject(category);
    return model;
  }

  @override
  Future delete(Product category) async {
    final query = Query<Product>(context)
      ..where((u) => u.id).equalTo(category.id);
    return query.delete();
  }

  @override
  Future edit(Product category) async {
    final query = Query<Product>(context)
      ..values.name = category.name
      ..where((u) => u.id).equalTo(category.id);

    return query.updateOne();
  }

  @override
  Future<List<Product>> getAll() {
    var query = Query<Product>(context);
    (query.join(set: (p) => p.categories)
          ..returningProperties((c) => [c.id, c.category]))
        .join(object: (c) => c.category);

    return query.fetch();
  }

  @override
  Future<Product> addCategory(Product product, Category category) async {
    await context.insertObject(
      ProductCategory()
        ..id = "${product.id}:${category.id}"
        ..product = product
        ..category = category,
    );

    var query = Query<Product>(context);
    query.where((p) => p.id).equalTo(product.id);
    (query.join(set: (p) => p.categories)
          ..returningProperties((c) => [c.id, c.category]))
        .join(object: (c) => c.category);

    return query.fetchOne();
  }
}
