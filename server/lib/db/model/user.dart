import 'package:aqueduct/aqueduct.dart';
import 'package:aqueduct/managed_auth.dart';
import 'package:domain_api/domain_api.dart';

class User extends ManagedObject<_User>
    implements _User, ManagedAuthResourceOwner<_User> {
  @Serialize(input: true, output: false)
  String password;
}

@Table(name: "_user")
class _User extends ResourceOwnerTableDefinition implements MUser {
  @Column(unique: true)
  String email;

  @Column(unique: true)
  String phone;

  @Column()
  int role;

  @override
  String get password => null;
}
