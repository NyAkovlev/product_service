import 'package:aqueduct/aqueduct.dart';
import 'package:domain_api/domain_api.dart';
import 'product_category_db.dart';

class Product extends ManagedObject<_Product> implements _Product {}

@Table(name: "product")
class _Product implements MProduct {
  @primaryKey
  int id;

  @Column(unique: true)
  String name;

  ManagedSet<ProductCategory> categories;
}
