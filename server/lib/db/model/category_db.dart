import 'package:aqueduct/aqueduct.dart';
import 'package:domain_api/domain_api.dart';
import 'product_category_db.dart';

class Category extends ManagedObject<_Category> implements _Category {}

@Table(name: "category")
class _Category implements MCategory {
  @primaryKey
  int id;

  @Column(unique: true)
  String name;

  ManagedSet<ProductCategory> products;
}
