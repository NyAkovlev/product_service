import 'package:aqueduct/aqueduct.dart';
import 'product.dart';
import 'category_db.dart';
import 'package:domain_api/domain_api.dart';

class ProductCategory extends ManagedObject<_ProductCategory>
    implements _ProductCategory {
}

@Table(name: "product_category")
class _ProductCategory implements MProductCategory {
  @Column(
    databaseType: ManagedPropertyType.string,
    primaryKey: true,
    indexed: false,
    autoincrement: false,
    unique: true,
  )
  String id;
  @Relate(#categories)
  Product product;
  @Relate(#products)
  Category category;
}
